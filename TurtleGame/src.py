from turtle import *
from random import randint
import time

def playGame():
    franklin = Turtle()
    franklin.color('red')
    franklin.shape('turtle')
    franklin.hideturtle()

    bob = Turtle()
    bob.color('blue')
    bob.shape('turtle')
    bob.hideturtle()

    tina = Turtle()
    tina.color('green')
    tina.shape('turtle')
    tina.hideturtle()

    showMenu()
    drawBoard()
    drawTurtles(franklin, bob, tina)
    startRace(franklin, bob, tina)

def showMenu():
    # print("WELCOME TO TURTLE RACES!")
    # print("Grab a friend (or a few) and guess which color turtle will win the race!")
    # print("Red = Franklin")
    # print("Blue = Bob")
    # print("Green = Tina")
    # print()
    # print("Ready? (press 'enter' to continue)")
    # input()
    pass

def drawTitle():
    penup()
    goto(-160, 190)
    write("Illegal Underground Turtle Racing",font=("Arial", 20, "italic"))
    
def drawBoard():
    drawTitle()
    speed(10)
    goto(-160, 140)

    for i in range(15):
        write(i)
        right(90)
        forward(10)
        pendown()
        forward(150)
        penup()
        backward(160)
        left(90)
        forward(20)
   
def drawTurtles(franklin, bob, tina):
    franklin.showturtle()
    franklin.penup()
    franklin.goto(-160, 100)
    franklin.pendown()

    bob.showturtle()
    bob.penup()
    bob.goto(-160, 50)
    bob.pendown()

    tina.showturtle()
    tina.penup()
    tina.goto(-160, 0)
    tina.pendown()

def startRace(franklin, bob, tina):
    for turn in range(200):
        if franklin.xcor() >= 140:
            penup()
            goto(-90, 150)
            color("red")
            write("FRANKLIN WINS!!!", font=("Arial",20,"normal"))
            print("FRANKLIN")
            break

        elif bob.xcor() >= 140:
            penup()
            goto(-90, 150)
            color("blue")
            write("BOB WINS!!!", font=("Arial",20,"normal"))
            print("BOB")
            break

        elif tina.xcor() >= 140:
            penup()
            goto(-90, 150)
            color("green")
            write("TINA WINS!!!", font=("Arial",20,"normal"))
            print("TINA")
            break
        
        franklin.forward(randint(1, 5))
        bob.forward(randint(1, 5))
        tina.forward(randint(1, 5))

